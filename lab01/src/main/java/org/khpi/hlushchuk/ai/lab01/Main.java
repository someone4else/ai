package org.khpi.hlushchuk.ai.lab01;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/main.fxml"));
        primaryStage.setScene(new Scene(root));

        primaryStage.setMinWidth(700);
        primaryStage.setMinHeight(400);
        primaryStage.setTitle("Лабораторная работа 1");

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
