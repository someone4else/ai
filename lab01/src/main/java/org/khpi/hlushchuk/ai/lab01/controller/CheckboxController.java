package org.khpi.hlushchuk.ai.lab01.controller;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import org.khpi.hlushchuk.ai.lab01.model.CheckboxValues;

import java.util.List;

public class CheckboxController {

    @FXML
    private List<List<CheckBox>> etalon1Checkboxes;
    @FXML
    private List<List<CheckBox>> etalon2Checkboxes;
    @FXML
    private List<List<CheckBox>> etalon3Checkboxes;
    @FXML
    private List<List<CheckBox>> etalon4Checkboxes;
    @FXML
    private List<List<CheckBox>> recognizableSymbolCheckboxes;

    public void initialize() {
        fillCheckboxes();
    }

    public CheckboxValues getCheckboxValues() {
        int[][][] etalonValues = {
                getEtalon1Values(),
                getEtalon2Values(),
                getEtalon3Values(),
                getEtalon4Values()};
        int[][] recognizableSymbolValues = getRecognizableSymbolValues();

        return new CheckboxValues(etalonValues, recognizableSymbolValues);
    }

    private int[][] getEtalon1Values() {
        return getCheckboxValues(etalon1Checkboxes);
    }

    private int[][] getEtalon2Values() {
        return getCheckboxValues(etalon2Checkboxes);
    }

    private int[][] getEtalon3Values() {
        return getCheckboxValues(etalon3Checkboxes);
    }

    private int[][] getEtalon4Values() {
        return getCheckboxValues(etalon4Checkboxes);
    }

    private int[][] getRecognizableSymbolValues() {
        return getCheckboxValues(recognizableSymbolCheckboxes);
    }

    private int[][] getCheckboxValues(List<List<CheckBox>> checkboxes) {
        int[][] result = new int[checkboxes.size()][];
        for (int i = 0; i < checkboxes.size(); i++) {
            List<CheckBox> currentRow = checkboxes.get(i);
            result[i] = new int[currentRow.size()];
            for (int j = 0; j < currentRow.size(); j++) {
                result[i][j] = currentRow.get(j).isSelected() ? 1 : 0;
            }
        }
        return result;
    }

    private void fillCheckboxes() {
        int[][] etalon1 = {
                {0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0},
                {0, 1, 1, 1, 0},
                {0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0}
        };
        fillCheckboxes(etalon1Checkboxes, etalon1);

        int[][] etalon2 = {
                {0, 0, 1, 0, 0},
                {0, 1, 0, 1, 0},
                {0, 1, 1, 1, 0},
                {0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0}
        };
        fillCheckboxes(etalon2Checkboxes, etalon2);

        int[][] etalon3 = {
                {0, 0, 1, 0, 0},
                {0, 1, 0, 1, 0},
                {0, 1, 0, 0, 0},
                {0, 1, 0, 1, 0},
                {0, 0, 1, 0, 0}
        };
        fillCheckboxes(etalon3Checkboxes, etalon3);

        int[][] etalon4 = {
                {0, 1, 1, 1, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0}
        };
        fillCheckboxes(etalon4Checkboxes, etalon4);

        int[][] symbol = {
                {0, 0, 1, 0, 0},
                {0, 1, 0, 1, 0},
                {0, 1, 0, 0, 0},
                {0, 1, 0, 1, 0},
                {0, 0, 1, 0, 0}
        };
        fillCheckboxes(recognizableSymbolCheckboxes, symbol);
    }

    private void fillCheckboxes(List<List<CheckBox>> checkboxes, int[][] values) {
        for (int i = 0; i < values.length; i++) {
            int[] row = values[i];
            for (int j = 0; j < row.length; j++) {
                checkboxes.get(i).get(j).setSelected(row[j] == 1);
            }
        }
    }
}
