package org.khpi.hlushchuk.ai.lab01.controller;

import javafx.fxml.FXML;
import org.khpi.hlushchuk.ai.lab01.model.CheckboxValues;

public class MainController {

    @FXML
    private CheckboxController checkboxController;
    @FXML
    private FormulaController formulaController;

    @FXML
    private void recognize() {
        CheckboxValues checkboxValues = checkboxController.getCheckboxValues();
        formulaController.executeCalculation(checkboxValues);
    }
}
