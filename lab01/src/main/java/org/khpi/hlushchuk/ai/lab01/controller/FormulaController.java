package org.khpi.hlushchuk.ai.lab01.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.khpi.hlushchuk.ai.lab01.model.CheckboxValues;
import org.khpi.hlushchuk.ai.lab01.util.FormulaResolver;

import java.util.List;
import java.util.function.Function;

public class FormulaController {

    @FXML
    private ImageView minkovskyFormulaImage;
    @FXML
    private List<TextField> minkovskyResults;

    @FXML
    private ImageView diffAbsSumFormulaImage;
    @FXML
    private List<TextField> diffAbsSumResults;

    @FXML
    private ImageView diffAbsSumCoeffFormulaImage;
    @FXML
    private List<TextField> diffAbsSumCoeffResults;

    @FXML
    private ImageView kendalFormulaImage;
    @FXML
    private List<TextField> kendalResults;

    @FXML
    private ImageView chebyshevFormulaImage;
    @FXML
    private List<TextField> chebyshevResults;

    @FXML
    private ImageView sokalSnifFormulaImage;
    @FXML
    private List<TextField> sokalSnifResults;

    @FXML
    private ImageView sokalMishnerFormulaImage;
    @FXML
    private List<TextField> sokalMishnerResults;

    @FXML
    private ImageView kulzhinskyFormulaImage;
    @FXML
    private List<TextField> kulzhinskyResults;

    @FXML
    private ImageView yulFormulaImage;
    @FXML
    private List<TextField> yulResults;


    public void initialize() {
        initializeImages();
    }

    public void executeCalculation(CheckboxValues checkboxValues) {
        calculate(checkboxValues, FormulaResolver::minkovsky, minkovskyResults);
        calculate(checkboxValues, FormulaResolver::diffAbsSum, diffAbsSumResults);
        calculate(checkboxValues, FormulaResolver::diffAbsSumCoeff, diffAbsSumCoeffResults);
        calculate(checkboxValues, FormulaResolver::kendal, kendalResults);
        calculate(checkboxValues, FormulaResolver::chebyshev, chebyshevResults);
        calculate(checkboxValues, FormulaResolver::sokalSnif, sokalSnifResults);
        calculate(checkboxValues, FormulaResolver::sokalMishner, sokalMishnerResults);
        calculate(checkboxValues, FormulaResolver::kulzhinsky, kulzhinskyResults);
        calculate(checkboxValues, FormulaResolver::yul, yulResults);
    }

    private void calculate(CheckboxValues checkboxValues,
                           Function<CheckboxValues, Double[]> calculationFunction,
                           List<TextField> resultFields) {
        Double[] result = calculationFunction.apply(checkboxValues);
        for (int i = 0; i < result.length; i++) {
            resultFields.get(i).setText(String.format("%.2f", result[i]));
        }
    }

    private void initializeImages() {
        minkovskyFormulaImage.setImage(new Image(getClass().getResourceAsStream("/img/formula1.3.png")));
        diffAbsSumFormulaImage.setImage(new Image(getClass().getResourceAsStream("/img/formula1.4.png")));
        diffAbsSumCoeffFormulaImage.setImage(new Image(getClass().getResourceAsStream("/img/formula1.7.png")));
        kendalFormulaImage.setImage(new Image(getClass().getResourceAsStream("/img/formula1.9.png")));
        chebyshevFormulaImage.setImage(new Image(getClass().getResourceAsStream("/img/formula1.10.png")));
        sokalSnifFormulaImage.setImage(new Image(getClass().getResourceAsStream("/img/formula1.25.png")));
        sokalMishnerFormulaImage.setImage(new Image(getClass().getResourceAsStream("/img/formula1.26.png")));
        kulzhinskyFormulaImage.setImage(new Image(getClass().getResourceAsStream("/img/formula1.27.png")));
        yulFormulaImage.setImage(new Image(getClass().getResourceAsStream("/img/formula1.28.png")));
    }
}
