package org.khpi.hlushchuk.ai.lab01.model;

public class CheckboxValues {

    int[][][] etalonValues;
    int[][] recognizableSymbolValues;

    public CheckboxValues() {
    }

    public CheckboxValues(int[][][] etalonValues, int[][] recognizableSymbolValues) {
        this.etalonValues = etalonValues;
        this.recognizableSymbolValues = recognizableSymbolValues;
    }

    public int[][][] getEtalonValues() {
        return etalonValues;
    }

    public void setEtalonValues(int[][][] etalonValues) {
        this.etalonValues = etalonValues;
    }

    public int[][] getRecognizableSymbolValues() {
        return recognizableSymbolValues;
    }

    public void setRecognizableSymbolValues(int[][] recognizableSymbolValues) {
        this.recognizableSymbolValues = recognizableSymbolValues;
    }
}
