package org.khpi.hlushchuk.ai.lab01.util;

import org.khpi.hlushchuk.ai.lab01.model.CheckboxValues;

import java.util.Arrays;
import java.util.function.IntBinaryOperator;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.pow;

public class FormulaResolver {

    private FormulaResolver() {
    }

    public static Double[] minkovsky(CheckboxValues values) {
        int[][][] etalonValues = values.getEtalonValues();
        int[][] symbol = values.getRecognizableSymbolValues();

        Double[] result = new Double[etalonValues.length];
        Arrays.fill(result, 0.0);

        for (int i = 0; i < etalonValues.length; i++) {
            int[][] etalon = etalonValues[i];
            for (int j = 0; j < etalon.length; j++) {
                for (int k = 0; k < etalon[j].length; k++) {
                    result[i] += pow((double) symbol[j][k] - etalon[j][k], 4);
                }
            }
        }

        for (int i = 0; i < result.length; i++) {
            result[i] = pow(result[i], 0.25);
        }

        return result;
    }

    public static Double[] diffAbsSum(CheckboxValues values) {
        int[][][] etalonValues = values.getEtalonValues();
        int[][] symbol = values.getRecognizableSymbolValues();

        Double[] result = new Double[etalonValues.length];
        Arrays.fill(result, 0.0);

        for (int i = 0; i < etalonValues.length; i++) {
            int[][] etalon = etalonValues[i];
            for (int j = 0; j < etalon.length; j++) {
                for (int k = 0; k < etalon[j].length; k++) {
                    result[i] += abs(symbol[j][k] - etalon[j][k]);
                }
            }
        }

        return result;
    }

    public static Double[] diffAbsSumCoeff(CheckboxValues values) {
        int[][][] etalonValues = values.getEtalonValues();
        int[][] symbol = values.getRecognizableSymbolValues();
        int[] factors = {2, 10};

        Double[] result = new Double[etalonValues.length];
        Arrays.fill(result, 0.0);

        for (int i = 0; i < etalonValues.length; i++) {
            int[][] etalon = etalonValues[i];
            for (int j = 0; j < etalon.length; j++) {
                for (int k = 0; k < etalon[j].length; k++) {
                    result[i] += factors[j % 2] * abs(symbol[j][k] - etalon[j][k]);
                }
            }
        }

        return result;
    }

    public static Double[] kendal(CheckboxValues values) {
        int[][][] etalonValues = values.getEtalonValues();
        int[][] symbol = values.getRecognizableSymbolValues();

        Double[] result = new Double[etalonValues.length];
        Arrays.fill(result, 0.0);

        for (int i = 0; i < etalonValues.length; i++) {
            int[][] etalon = etalonValues[i];

            int[] etalonFlattened = flatten(etalon);
            int[] symbolFlattened = flatten(symbol);

            int n = etalonFlattened.length;

            double outerSum = 0.0;
            for (int q = 0; q < n - 1; q++) {
                double innerSum = 0.0;
                for (int k = q + 1; k < n; k++) {
                    int symbolDelta = Integer.compare(symbolFlattened[q], symbolFlattened[k]);
                    int etalonDelta = Integer.compare(etalonFlattened[q], symbolFlattened[k]);
                    innerSum += symbolDelta * etalonDelta;
                }
                outerSum += innerSum;
            }

            result[i] = 1 - 2.0 / (n * (n - 1)) * outerSum;
        }

        return result;
    }

    public static Double[] chebyshev(CheckboxValues values) {
        int[][][] etalonValues = values.getEtalonValues();
        int[][] symbol = values.getRecognizableSymbolValues();

        Double[] result = new Double[etalonValues.length];
        Arrays.fill(result, 0.0);

        for (int i = 0; i < etalonValues.length; i++) {
            int[][] etalon = etalonValues[i];

            double maxDifference = 0.0;
            for (int j = 0; j < etalon.length; j++) {
                for (int k = 0; k < etalon[j].length; k++) {
                    maxDifference = max(maxDifference, abs(symbol[j][k] - etalon[j][k]));
                }
            }
            result[i] = maxDifference;
        }

        return result;
    }

    public static Double[] sokalSnif(CheckboxValues values) {
        int[][][] etalonValues = values.getEtalonValues();
        int[][] symbol = values.getRecognizableSymbolValues();

        Double[] result = new Double[etalonValues.length];
        Arrays.fill(result, 0.0);

        for (int i = 0; i < etalonValues.length; i++) {
            int[][] etalon = etalonValues[i];
            double a = var(symbol, etalon, FormulaResolver::a);
            double g = var(symbol, etalon, FormulaResolver::g);
            double h = var(symbol, etalon, FormulaResolver::h);

            if (a == 0 && g + h == 0) {
                result[i] = 0.0;
            } else {
                result[i] = a / (a + 2.0 * (g + h));
            }
        }

        return result;
    }

    public static Double[] sokalMishner(CheckboxValues values) {
        int[][][] etalonValues = values.getEtalonValues();
        int[][] symbol = values.getRecognizableSymbolValues();

        Double[] result = new Double[etalonValues.length];
        Arrays.fill(result, 0.0);

        for (int i = 0; i < etalonValues.length; i++) {
            int[][] etalon = etalonValues[i];
            double a = var(symbol, etalon, FormulaResolver::a);
            double b = var(symbol, etalon, FormulaResolver::b);

            result[i] = (a + b) / pow(etalon.length, 2);
        }

        return result;
    }

    public static Double[] kulzhinsky(CheckboxValues values) {
        int[][][] etalonValues = values.getEtalonValues();
        int[][] symbol = values.getRecognizableSymbolValues();

        Double[] result = new Double[etalonValues.length];
        Arrays.fill(result, 0.0);

        for (int i = 0; i < etalonValues.length; i++) {
            int[][] etalon = etalonValues[i];
            double a = var(symbol, etalon, FormulaResolver::a);
            double g = var(symbol, etalon, FormulaResolver::g);
            double h = var(symbol, etalon, FormulaResolver::h);

            if (g + h == 0) {
                result[i] = 0.0;
            } else {
                result[i] = a / (g + h);
            }
        }

        return result;
    }

    public static Double[] yul(CheckboxValues values) {
        int[][][] etalonValues = values.getEtalonValues();
        int[][] symbol = values.getRecognizableSymbolValues();

        Double[] result = new Double[etalonValues.length];
        Arrays.fill(result, 0.0);

        for (int i = 0; i < etalonValues.length; i++) {
            int[][] etalon = etalonValues[i];
            double a = var(symbol, etalon, FormulaResolver::a);
            double b = var(symbol, etalon, FormulaResolver::b);
            double g = var(symbol, etalon, FormulaResolver::g);
            double h = var(symbol, etalon, FormulaResolver::h);

            if (a * b + g * h == 0) {
                result[i] = 0.0;
            } else {
                result[i] = (a * b - g * h) / (a * b + g * h);
            }
        }

        return result;
    }

    private static int var(int[][] first, int[][] second, IntBinaryOperator function) {
        int result = 0;

        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < first[i].length; j++) {
                result += function.applyAsInt(first[i][j], second[i][j]);
            }
        }

        return result;
    }

    private static int a(int first, int second) {
        return first * second;
    }

    private static int b(int first, int second) {
        return (1 - first) * (1 - second);
    }

    private static int g(int first, int second) {
        return first * (1 - second);
    }

    private static int h(int first, int second) {
        return (1 - first) * second;
    }

    private static int[] flatten(int[][] array) {
        return Arrays.stream(array)
                .flatMapToInt(Arrays::stream)
                .toArray();
    }
}
